﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ParentController : MonoBehaviour {
    [Header("General")] 
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private Text text = null;
    public Parent parent;
    public List<Student> students;

    [Header("Moving")]
    [SerializeField] private GameObject[] screens = null;
    [SerializeField] private GameObject[] buttons = null;
    
    // Start is called before the first frame update
    void Start() {
        students = new List<Student>();
        StartCoroutine(initialize());
    }
    
    IEnumerator initialize() {
        yield return new WaitUntil(() => tconstants.isLoaded == true);

        foreach (var p in tconstants.parents) {
            if (p.email.Equals(FirebaseAuth.DefaultInstance.CurrentUser.Email)) {
                parent = p;
                break;
            }
        }

        foreach (var s in tconstants.students) {
            if (s.parentEmail.Equals(parent.email)) {
                students.Add(s);
            }
        }

        text.text = parent.fullName + "  ->  Debt: " + parent.debt;
    }
    
    public void changeScreen(int i) {
        foreach (var s in screens) s.SetActive(false);
        screens[i].SetActive(true);

        buttons[0].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[0].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        buttons[1].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[1].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        
        buttons[i].GetComponent<Image>().color = new Color32(59, 107, 171, 255);
        buttons[i].transform.GetChild(0).GetComponent<Text>().color = new Color32(59, 107, 171, 255);
    }

    //account UI
    public void signOut() {
        FirebaseAuth.DefaultInstance.SignOut();
        SceneManager.LoadScene("Login");
    }

    public void reload() {
        SceneManager.LoadScene("Parent");
    }
}