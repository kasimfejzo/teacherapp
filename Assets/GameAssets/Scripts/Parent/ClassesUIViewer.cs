﻿using System;
using System.Collections;
using Firebase;
using Firebase.Unity.Editor;
using UnityEngine;
using UnityEngine.UI;

public class ClassesUIViewer : MonoBehaviour {
    [Header("General")]
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private GameObject classField = null;
    [SerializeField] private Transform classesListContent = null;
    [SerializeField] private ParentController pcontroller = null;
    [SerializeField] private StudentController scontroller = null;

    [Header("ClassUI")]
    [SerializeField] private GameObject classUI = null;
    [SerializeField] private Transform sessionsListContent = null;

    [Header("SessionUI")]
    [SerializeField] private GameObject sessionUI = null;
    [SerializeField] private Transform sessionTabContent = null;
    [SerializeField] private GameObject[] tabButtons = null;

    [Header("SessionTabs")]
    [SerializeField] private GameObject gradesPopUp = null;
    [SerializeField] private Transform gradesContent = null;
    [SerializeField] private InputField[] gradeInputFields = null;
    
    [SerializeField] private GameObject studentANGField = null;
    [SerializeField] private GameObject assignmentsPopUp = null;
    [SerializeField] private Transform assignmentsContent = null;
    [SerializeField] private InputField[] assignmentInputFields = null;
    
    [SerializeField] private GameObject studentField = null;

    [Header("YouTube")]
    [SerializeField] private GameObject youTubeGO = null;
    [SerializeField] private GameObject mainCanvas = null;
    
    
    private int currentClassIndex;
    private int currentSessionIndex;
    
    // Start is called before the first frame update
    void Start() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(tconstants.DATABASE_URL);
        StartCoroutine(listAllClasses());

        currentClassIndex = -1;
        currentSessionIndex = -1;
    }
    
    IEnumerator listAllClasses() {
        yield return new WaitUntil(() => tconstants.isLoaded == true);
        clearContent(classesListContent);

        int index = 0;
        foreach (var c in tconstants.classes) {
            bool list = false;
            if (pcontroller != null) {
                foreach (var s in pcontroller.students) {
                    if (c.students.Contains(s.email)) list = true;
                }
            }
            else if (scontroller != null) {
                if (c.students.Contains(scontroller.student.email)) list = true;
            }
            if (!list) continue;
            
            GameObject go = Instantiate(classField, classesListContent);
            go.transform.GetChild(0).GetComponent<Text>().text = c.className;
            int i = index;
            //edit button
            go.transform.GetChild(1).gameObject.SetActive(false);
            //edit button -- end
            //remove button
            go.transform.GetChild(2).gameObject.SetActive(false);
            //remove button -- end
            go.name = index.ToString();
            go.GetComponent<Button>().onClick.AddListener(delegate { openClass(i); });

            ++index;
        }
    }
    private void clearContent(Transform content) {
        try {
            int index = 0;
            Transform child = content.transform.GetChild(index);
            while (child != null) {
                Destroy(child.gameObject);
                ++index;
                child = content.transform.GetChild(index);
            }
        }
        catch (Exception e) {
            Debug.Log(e);
        }
    }

    //ClassesUI
    //ClassField buttons
    private void openClass(int classIndex) { 
        clearContent(sessionsListContent);
        classUI.SetActive(true);
        setupClass(classIndex);
    }
    //ClassField buttons -- end
    //ClassesUI -- end
    
    //ClassUI
    private void setupClass(int classIndex) {
        classUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = tconstants.classes[classIndex].className;
        int index = 0;
        foreach (var s in tconstants.classes[classIndex].sessions) {
            GameObject go = Instantiate(classField, sessionsListContent); //using classfield cause it's the same shit
            go.transform.GetChild(0).GetComponent<Text>().text = s.sessionName;
            int i = index;
            //edit button
            go.transform.GetChild(1).gameObject.SetActive(false);
            //edit button -- end
            //remove button
            go.transform.GetChild(2).gameObject.SetActive(false);
            //remove button -- end
            go.name = index.ToString();
            go.GetComponent<Button>().onClick.AddListener(delegate { openSession(i, classIndex); });
            
            ++index;
        }
    }

    public void closeClass() {
        classUI.SetActive(false);
    }
    
    //SessionField buttons
    private void openSession(int sessionIndex, int classIndex) {
        sessionUI.SetActive(true);
        setupSession(sessionIndex, classIndex);
    }
    //SessionField buttons -- end
    //ClassUI -- end
    
    //SessionUI

    private void setupSession(int sessionIndex, int classIndex) {
        currentClassIndex = classIndex;
        currentSessionIndex = sessionIndex;
        sessionUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = tconstants.classes[classIndex].sessions[sessionIndex].sessionName;
        switchSessionTab(0);
    }

    public void switchSessionTab(int tabIndex) {
        if (currentClassIndex == -1 || currentSessionIndex == -1) return;
        
        clearContent(sessionTabContent);
        switch (tabIndex) {
            case 0:
                setupAttachmentsTab();
                break;
            case 1:
                setupAttendanceTab();
                break;
            case 2:
                setupGradesTab();
                break;
            case 3:
                setupAssignmentsTab();
                break;
        }

        foreach (var btn in tabButtons) {
            btn.GetComponent<Image>().color = new Color32(154, 154, 154, 255);
            btn.transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        }
        tabButtons[tabIndex].GetComponent<Image>().color = new Color32(253, 121, 73, 255);
        tabButtons[tabIndex].transform.GetChild(0).GetComponent<Text>().color = new Color32(253, 121, 73, 255);
    }

    //attachmentTab
    private void setupAttachmentsTab() { //still using classField cause same shit just disable remove option.
        GameObject go1 = Instantiate(classField, sessionTabContent);
        go1.transform.GetChild(0).GetComponent<Text>().text = "Session documents";
        //edit button
        go1.transform.GetChild(1).transform.gameObject.SetActive(false);
        //edit button -- end
        //remove button
        go1.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url1 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].documentsUrl;
        go1.GetComponent<Button>().onClick.AddListener(delegate { Application.OpenURL(url1); });
        
        
        GameObject go2 = Instantiate(classField, sessionTabContent);
        go2.transform.GetChild(0).GetComponent<Text>().text = "Zoom meeting";
        //edit button
        go2.transform.GetChild(1).transform.gameObject.SetActive(false);
        //edit button -- end
        //remove button
        go2.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url2 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].zoomUrl;
        go2.GetComponent<Button>().onClick.AddListener(delegate { Application.OpenURL(url2); });
        
        
        GameObject go3 = Instantiate(classField, sessionTabContent);
        go3.transform.GetChild(0).GetComponent<Text>().text = "Youtube video";
        //edit button
        go3.transform.GetChild(1).transform.gameObject.SetActive(false);
        //edit button -- end
        //remove button
        go3.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url3 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].youtubeUrl;
        go3.GetComponent<Button>().onClick.AddListener(delegate { openVideo(url3); });
    }
    //attachmentTab -- end
    
    //attendanceTab
    private void setupAttendanceTab() {
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            
            bool list = false;
            if (pcontroller != null) {
                foreach (var s in pcontroller.students) {
                    if (tconstants.classes[currentClassIndex].students[i].Equals(s.email)) list = true;
                }
            }
            else if (scontroller != null) {
                if (tconstants.classes[currentClassIndex].students[i].Equals(scontroller.student.email)) list = true;
            }
            if (!list) continue;
            
            GameObject go = Instantiate(studentField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            go.transform.GetChild(1).GetComponent<Toggle>().isOn = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].attendance[i];
            go.transform.GetChild(1).GetComponent<Toggle>().interactable = false;
            go.name = i.ToString();
        }
    }
    
    //attendanceTab -- end
    
    //grades
    private void setupGradesTab() {
        int testIndex = 0;
        foreach (var test in tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests) {
            GameObject go = Instantiate(classField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = test.testName;
            int i = testIndex;
            go.transform.GetComponent<Button>().onClick.AddListener(delegate { showGradesPopUp(i); });
            go.transform.GetChild(1).transform.gameObject.SetActive(false);
            go.transform.GetChild(2).transform.gameObject.SetActive(false);
            ++testIndex;
        }
    }
    
    private void showGradesPopUp(int testIndex) {
        gradesPopUp.SetActive(true);

        gradeInputFields[0].text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].testName;
        gradeInputFields[1].text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].maxGrade.ToString();

        gradeInputFields[0].interactable = false;
        gradeInputFields[1].interactable = false;

        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            bool list = false;
            if (pcontroller != null) {
                foreach (var s in pcontroller.students) {
                    if (tconstants.classes[currentClassIndex].students[i].Equals(s.email)) list = true;
                }
            }
            else if (scontroller != null) {
                if (tconstants.classes[currentClassIndex].students[i].Equals(scontroller.student.email)) list = true;
            }
            if (!list) continue;
            
            GameObject go = Instantiate(studentANGField, gradesContent);
            go.transform.GetChild(1).GetComponent<InputField>().interactable = false;
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            if (tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].studentGrades[i] == -1) continue;
            go.transform.GetChild(1).GetComponent<InputField>().text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].studentGrades[i].ToString();
        }
    }

    public void closeGradesPopUp() {
        gradesPopUp.SetActive(false);
        clearContent(gradesContent);
    }
    //grades -- end
    
    //assignments
    private void setupAssignmentsTab() {
        int assIndex = 0;
        foreach (var ass in tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments) {
            GameObject go = Instantiate(classField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = ass.testName;
            int i = assIndex;
            go.transform.GetComponent<Button>().onClick.AddListener(delegate { showAssignmentsPopUp(i); });
            go.transform.GetChild(1).transform.gameObject.SetActive(false);
            go.transform.GetChild(2).transform.gameObject.SetActive(false);
            ++assIndex;
        }
    }
    
    private void showAssignmentsPopUp(int assIndex) {
        gradesPopUp.SetActive(true);

        assignmentInputFields[0].text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].testName;
        assignmentInputFields[1].text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].maxGrade.ToString();

        assignmentInputFields[0].interactable = false;
        assignmentInputFields[1].interactable = false;

        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            bool list = false;
            if (pcontroller != null) {
                foreach (var s in pcontroller.students) {
                    if (tconstants.classes[currentClassIndex].students[i].Equals(s.email)) list = true;
                }
            }
            else if (scontroller != null) {
                if (tconstants.classes[currentClassIndex].students[i].Equals(scontroller.student.email)) list = true;
            }
            if (!list) continue;
            
            GameObject go = Instantiate(studentANGField, assignmentsContent);
            go.transform.GetChild(1).GetComponent<InputField>().interactable = false;
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            if (tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].studentGrades[i] == -1) continue;
            go.transform.GetChild(1).GetComponent<InputField>().text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].studentGrades[i].ToString();
        }
    }

    public void closeAssignmentsPopUp() {
        assignmentsPopUp.SetActive(false);
        clearContent(assignmentsContent);
    }

    public void closeSession() {
        sessionUI.SetActive(false);
        currentClassIndex = -1;
        currentSessionIndex = -1;
    }
    
    //SessionUI -- end
    
    //yt
    private void openVideo(string url) {
        youTubeGO.SetActive(true);
        mainCanvas.SetActive(false);
        youTubeGO.GetComponent<YoutubePlayer>().youtubeUrl = url;
        youTubeGO.GetComponent<YoutubePlayer>().PlayYoutubeVideo(url);
    }
    public void closeVideoPlayer() {
        youTubeGO.SetActive(false);
        youTubeGO.GetComponent<YoutubePlayer>().Stop();
        mainCanvas.SetActive(true);
    }
    //yt -- end
}
