﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase;
using UnityEngine;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AuthController : MonoBehaviour {
    [SerializeField] private InputField emailInput = null;
    [SerializeField] private InputField passwordInput = null;
    [SerializeField] private Toggle rememberMe = null;
    
    private List<Parent> parents;
    private List<Student> students;
    
    private DatabaseReference databaseReference;
    private static string DATABASE_URL = "https://teacherapp-4b5fc.firebaseio.com/";

    private bool loggedIn;

    private void Start() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(DATABASE_URL);
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
        
        if(PlayerPrefs.GetInt("RememberMe", 0) == 1) if (PlayerPrefs.HasKey("email")) emailInput.text = PlayerPrefs.GetString("email");
        if(PlayerPrefs.GetInt("RememberMe", 0) == 1) if (PlayerPrefs.HasKey("password")) passwordInput.text = PlayerPrefs.GetString("password");
        if (PlayerPrefs.HasKey("RememberMe")) {
            if (PlayerPrefs.GetInt("RememberMe") == 1) rememberMe.isOn = true;
            else if (PlayerPrefs.GetInt("RememberMe") == 0) rememberMe.isOn = false;
        }
        
        loggedIn = false;

        load();
    }

    private void load() {
        FirebaseDatabase.DefaultInstance.GetReferenceFromUrl(DATABASE_URL).GetValueAsync().ContinueWith((task => {
            if (task.IsCanceled) {}

            if (task.IsFaulted) {}

            if (task.IsCompleted) {
                DataSnapshot snapshot = task.Result;
                
                parents = new List<Parent>();
                students = new List<Student>();

                foreach (var child in snapshot.Child("Parent").Children) {
                    string pData = child.GetRawJsonValue();
                    parents.Add(JsonUtility.FromJson<Parent>(pData));
                }
                
                foreach (var child in snapshot.Child("Student").Children) {
                    string pData = child.GetRawJsonValue();
                    students.Add(JsonUtility.FromJson<Student>(pData));
                }
            }
            
        }));
    }

    private void FixedUpdate() {
        if (!loggedIn) return;
        
        if(rememberMe.isOn) PlayerPrefs.SetInt("RememberMe", 1);
        if(!rememberMe.isOn) PlayerPrefs.SetInt("RememberMe", 0);
        
        if(rememberMe.isOn) PlayerPrefs.SetString("email", emailInput.text);
        if(rememberMe.isOn) PlayerPrefs.SetString("password", passwordInput.text);

        if (emailInput.text.Contains("@teacher.com")) SceneManager.LoadScene("Teacher");
        foreach (var s in students) if (s.email.Equals(emailInput.text)) SceneManager.LoadScene("Student");
        foreach (var p in parents) if (p.email.Equals(emailInput.text)) SceneManager.LoadScene("Parent");
    }

    public void LogIn() {
        FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(emailInput.text, passwordInput.text).ContinueWith((
            task => {
                if (task.IsCanceled) {
                    return;
                }

                if (task.IsFaulted) {
                    Firebase.FirebaseException e =
                        task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                    LogErrorMessage((AuthError) e.ErrorCode);
                    return;
                }

                if (task.IsCompleted) {
                    print("Login successfull!");
                    loggedIn = true;
                }
                
            }));
    }

    private void LogErrorMessage(AuthError authError) {
        loggedIn = false;
        print(authError.ToString());
    }
}
