﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class Student {
    public string email;
    public string fullName;
    public string phoneNumber;
    public string parentEmail;
    //public List<string> messages;

    public Student(string pemail, string pname, string pn, string parentemail) {
        email = pemail;
        fullName = pname;
        phoneNumber = pn;
        parentEmail = parentemail;
        //messages = new List<string>();
    } 
}
