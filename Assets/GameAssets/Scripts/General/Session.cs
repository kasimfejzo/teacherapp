﻿using System;
using System.Collections.Generic;

[Serializable]
public class Session {
    public string sessionName;
    public string sessionDescription;
    
    //attachments
    public string documentsUrl;
    public string zoomUrl;
    public string youtubeUrl;
    
    //attendance
    public List<bool> attendance;
    
    //grades(tests) tab
    public List<Test> tests;
    
    //assignments tab
    public List<Test> assignments;
    public Session(string name, string des, int numberOfStudents) {
        sessionName = name;
        sessionDescription = des;

        documentsUrl = "";
        zoomUrl = "";
        youtubeUrl = "";

        attendance = new List<bool>();
        for(int i = 0; i < numberOfStudents; ++i) attendance.Add(false);

        tests = new List<Test>();

        assignments = new List<Test>();
    }
}
