﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Test {
    public string testName;
    public int maxGrade;
    public List<int> studentGrades;

    public Test(string name, int max, int numberOfStudents) {
        testName = name;
        maxGrade = max;

        studentGrades = new List<int>();
        for(int i = 0; i < numberOfStudents; ++i) studentGrades.Add(-1);
    }
}
