﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class Class {
    public string className;
    public List<string> students;
    public List<Session> sessions;
    
    //public List<string> eventboard;

    public Class(string name) {
        className = name;
        sessions = new List<Session>();
        students = new List<string>();
        
        //eventboard = new List<string>();
    }
}