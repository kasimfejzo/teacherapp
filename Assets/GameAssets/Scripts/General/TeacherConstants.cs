﻿using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;
using UnityEngine.UI;

public class TeacherConstants : MonoBehaviour {
    
    public DatabaseReference databaseReference;
    public string DATABASE_URL = "https://teacherapp-4b5fc.firebaseio.com/";

    public List<Class> classes;
    public List<Parent> parents;
    public List<Student> students;

    public bool isLoaded;

    [SerializeField] private Image ToastMessage = null;
    
    // Start is called before the first frame update
    void Start() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(DATABASE_URL);
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
        showToast("Welcome back!", 2);
        isLoaded = false;
        
        load();
    }

    public void load() {
        FirebaseDatabase.DefaultInstance.GetReferenceFromUrl(DATABASE_URL).GetValueAsync().ContinueWith((task => {
            if (task.IsCanceled) {}

            if (task.IsFaulted) {}

            if (task.IsCompleted) {
                DataSnapshot snapshot = task.Result;
                
                classes = new List<Class>();
                parents = new List<Parent>();
                students = new List<Student>();

                foreach (var child in snapshot.Child("Class").Children) {
                    string pData = child.GetRawJsonValue();
                    classes.Add(JsonUtility.FromJson<Class>(pData));
                }
                
                foreach (var child in snapshot.Child("Parent").Children) {
                    string pData = child.GetRawJsonValue();
                    parents.Add(JsonUtility.FromJson<Parent>(pData));
                }
                
                foreach (var child in snapshot.Child("Student").Children) {
                    string pData = child.GetRawJsonValue();
                    students.Add(JsonUtility.FromJson<Student>(pData));
                }

                isLoaded = true;
            }
            
        }));
    }
    
    //Toast
    public void showToast(string text, int duration) {
        StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text, int duration) {
        Color orginalColor = ToastMessage.color;

        ToastMessage.transform.GetChild(0).GetComponent<Text>().text = text;
        ToastMessage.enabled = true;
        ToastMessage.transform.GetChild(0).GetComponent<Text>().enabled = true;

        //Fade in
        yield return fadeInAndOut(ToastMessage, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration) {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut(ToastMessage, false, 0.5f);

        ToastMessage.enabled = false;
        ToastMessage.transform.GetChild(0).GetComponent<Text>().enabled = false;
        ToastMessage.color = orginalColor;
    }

    IEnumerator fadeInAndOut(Image target, bool fadeIn, float duration) {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn) {
            a = 0f;
            b = 1f;
        }
        else {
            a = 1f;
            b = 0f;
        }

        Color currentColor = Color.clear;
        float counter = 0f;

        while (counter < duration) {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            target.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
            yield return null;
        }
    }
}
