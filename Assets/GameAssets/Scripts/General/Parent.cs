﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class Parent {
    public string email;
    public string fullName;
    public string phoneNumber;
    public string debt;
    
    public Parent(string pemail, string pname, string pn) {
        email = pemail;
        fullName = pname;
        phoneNumber = pn;
        debt = "$0";
    } 
}
