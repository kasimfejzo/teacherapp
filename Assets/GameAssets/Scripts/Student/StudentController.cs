﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StudentController : MonoBehaviour {
    [Header("General")] 
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private Text text = null;
    public Student student;

    [Header("Moving")]
    [SerializeField] private GameObject[] screens = null;
    [SerializeField] private GameObject[] buttons = null;
    
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(initialize());
    }
    
    IEnumerator initialize() {
        yield return new WaitUntil(() => tconstants.isLoaded == true);

        foreach (var s in tconstants.students) {
            if (s.email.Equals(FirebaseAuth.DefaultInstance.CurrentUser.Email)) {
                student = s;
                break;
            }
        }

        text.text = student.fullName;
    }
    
    public void changeScreen(int i) {
        foreach (var s in screens) s.SetActive(false);
        screens[i].SetActive(true);

        buttons[0].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[0].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        buttons[1].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[1].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        
        buttons[i].GetComponent<Image>().color = new Color32(59, 107, 171, 255);
        buttons[i].transform.GetChild(0).GetComponent<Text>().color = new Color32(59, 107, 171, 255);
    }

    //account UI
    public void signOut() {
        FirebaseAuth.DefaultInstance.SignOut();
        SceneManager.LoadScene("Login");
    }

    public void reload() {
        SceneManager.LoadScene("Student");
    }
}
