﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TeacherController : MonoBehaviour {
    [SerializeField] private GameObject[] screens = null;
    [SerializeField] private GameObject[] buttons = null;

    public void changeScreen(int i) {
        foreach (var s in screens) s.SetActive(false);
        screens[i].SetActive(true);

        buttons[0].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[0].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        buttons[1].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[1].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        buttons[2].GetComponent<Image>().color = new Color32(154, 154, 154, 255);
        buttons[2].transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        
        buttons[i].GetComponent<Image>().color = new Color32(59, 107, 171, 255);
        buttons[i].transform.GetChild(0).GetComponent<Text>().color = new Color32(59, 107, 171, 255);
    }

    //account UI
    public void signOut() {
        FirebaseAuth.DefaultInstance.SignOut();
        SceneManager.LoadScene("Login");
    }

    public void reload() {
        SceneManager.LoadScene("Teacher");
    }
}
