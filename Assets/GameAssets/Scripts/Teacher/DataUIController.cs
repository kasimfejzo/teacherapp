﻿using System;
using System.Collections;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.UI;

public class DataUIController : MonoBehaviour {
    
    [Header("General")]
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private GameObject[] tabButtons = null;
    [SerializeField] private GameObject addButton = null;
    [SerializeField] private Transform content = null;
    [SerializeField] private GameObject personField = null;

    private bool closePopUps;
    
    [Header("Student")] 
    [SerializeField] private GameObject ANSPopUp = null;
    [SerializeField] private GameObject ANSBackButton = null;
    [SerializeField] private InputField ANSEmailField = null;
    [SerializeField] private InputField ANSPasswordField = null;
    [SerializeField] private InputField ANSFullNameField = null;
    [SerializeField] private InputField ANSPhoneNumberField = null;
    [SerializeField] private InputField ANSParentEmailField = null;
    
    [Header("Parent")] 
    [SerializeField] private GameObject ANPPopUp = null;
    [SerializeField] private GameObject ANPBackButton = null;
    [SerializeField] private InputField ANPEmailField = null;
    [SerializeField] private InputField ANPPasswordField = null;
    [SerializeField] private InputField ANPFullNameField = null;
    [SerializeField] private InputField ANPPhoneNumberField = null;
    [SerializeField] private InputField ANPDebtField = null;
    
    private int tabToOpen = 0;


    // Start is called before the first frame update
    void Start() {
        closePopUps = false;
    }

    private void Update() {
        if (closePopUps) {
            ANSBackButton.SetActive(true);
            closeANSPopUp();
            
            ANPBackButton.SetActive(true);
            closeANPPopUp();
            
            closePopUps = false;
            StartCoroutine(waitAndLoad(tabToOpen));
        }
    }

    private void clearContent(Transform con) {
        try {
            int index = 0;
            Transform child = con.transform.GetChild(index);
            while (child != null) {
                Destroy(child.gameObject);
                ++index;
                child = con.transform.GetChild(index);
            }
        }
        catch (Exception e) {
            Debug.Log(e);
        }
    }

    public void switchTab(int tabIndex) {
        clearContent(content);

        addButton.GetComponent<Button>().onClick.RemoveAllListeners();
        addButton.transform.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
        switch (tabIndex) {
            case 0:
                addButton.GetComponent<Button>().onClick.AddListener(delegate { showANSPopUp(); });
                addButton.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { showANSPopUp(); });
                setupStudentsTab();
                break;
            case 1:
                addButton.GetComponent<Button>().onClick.AddListener(delegate { showANPPopUp(); });
                addButton.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { showANPPopUp(); });
                setupParentsTab();
                break;
        }
        
        foreach (var btn in tabButtons) {
            btn.GetComponent<Image>().color = new Color32(154, 154, 154, 255);
            btn.transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        }
        tabButtons[tabIndex].GetComponent<Image>().color = new Color32(253, 121, 73, 255);
        tabButtons[tabIndex].transform.GetChild(0).GetComponent<Text>().color = new Color32(253, 121, 73, 255);
    }
    
    //studentsTab
    private void setupStudentsTab() {
        int index = 0;
        foreach (var s in tconstants.students) {
            GameObject go = Instantiate(personField, content);
            go.transform.GetChild(0).GetComponent<Text>().text = s.email;
            int i = index;
            //edit button
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editStudent(i); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editStudent(i); });
            //edit button -- end
            //remove button
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeStudent(i); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { removeStudent(i); });
            //remove button -- end
            go.name = index.ToString();

            ++index;
        }
    }

    private void showANSPopUp() {
        ANSPopUp.SetActive(true);
    }

    public void saveStudent() {
        if (ANSEmailField.text.Equals("") || ANSPasswordField.text.Equals("") || ANSFullNameField.text.Equals("")) {
            tconstants.showToast("You need to input first 3 fields in order to continue!", 2);
            return;
        }

        if (!ANSEmailField.interactable) {
            saveStudentToDB();
            return;
        }

        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(ANSEmailField.text, ANSPasswordField.text)
            .ContinueWith((
                task => {
                    if (task.IsCanceled) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsFaulted) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsCompleted) {
                        print("Registration succesfull!");
                        saveStudentToDB();
                    }
                }));
    }
    
    private void LogErrorMessage(AuthError authError) {
        print(authError.ToString());
    }

    private void saveStudentToDB() {
        Student student = new Student(ANSEmailField.text, ANSFullNameField.text, ANSPhoneNumberField.text, ANSParentEmailField.text);
        string jsonData = JsonUtility.ToJson(student);

        string mail = ANSEmailField.text;
        string mailToAdd = mail.Replace(".", " ");
        tconstants.databaseReference.Child("Student").Child(ANSFullNameField.text + " -> " + mailToAdd).SetRawJsonValueAsync(jsonData);
        tconstants.isLoaded = false;
        tconstants.load();
        closePopUps = true;
        tabToOpen = 0;
    }

    public void closeANSPopUp() {
        ANSPopUp.SetActive(false);
        ANSEmailField.text = "";
        ANSPasswordField.text = "";
        ANSFullNameField.text = "";
        ANSPhoneNumberField.text = "";
        ANSParentEmailField.text = "";
        
        ANSEmailField.interactable = true;
        ANSPasswordField.interactable = true;
        ANSFullNameField.interactable = true;
    }

    private void editStudent(int studentIndex) {
        showANSPopUp();
        ANSBackButton.SetActive(false);

        ANSEmailField.text = tconstants.students[studentIndex].email;
        ANSPasswordField.text = "***";
        ANSFullNameField.text = tconstants.students[studentIndex].fullName;

        ANSEmailField.interactable = false;
        ANSPasswordField.interactable = false;
        ANSFullNameField.interactable = false;

        ANSParentEmailField.text = tconstants.students[studentIndex].parentEmail;
        ANSPhoneNumberField.text = tconstants.students[studentIndex].phoneNumber;
    }

    private void removeStudent(int studentIndex) {
        string mail = tconstants.students[studentIndex].email;
        string mailToAdd = mail.Replace(".", " ");
        tconstants.databaseReference.Child("Student").Child(tconstants.students[studentIndex].fullName + " -> " + mailToAdd).SetRawJsonValueAsync(null);
        tconstants.isLoaded = false;
        tconstants.load();
        
        StartCoroutine(waitAndLoad(0));
    }
    //studentsTab -- end
    
    //studentsTab
    private void setupParentsTab() {
        int index = 0;
        foreach (var s in tconstants.parents) {
            GameObject go = Instantiate(personField, content);
            go.transform.GetChild(0).GetComponent<Text>().text = s.email;
            int i = index;
            //edit button
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editParent(i); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editParent(i); });
            //edit button -- end
            //remove button
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeParent(i); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { removeParent(i); });
            //remove button -- end
            go.name = index.ToString();

            ++index;
        }
    }

    private void showANPPopUp() {
        ANPPopUp.SetActive(true);
    }

    public void saveParent() {
        if (ANPEmailField.text.Equals("") || ANPPasswordField.text.Equals("") || ANPFullNameField.text.Equals("")) {
            tconstants.showToast("You need to input first 3 fields in order to continue!", 2);
            return;
        }

        if (!ANPEmailField.interactable) {
            saveParentToDB();
            return;
        }

        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(ANPEmailField.text, ANPPasswordField.text)
            .ContinueWith((
                task => {
                    if (task.IsCanceled) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsFaulted) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsCompleted) {
                        print("Registration succesfull!");
                        saveParentToDB();
                    }
                }));
    }

    private void saveParentToDB() {
        Parent parent = new Parent(ANPEmailField.text, ANPFullNameField.text, ANSPhoneNumberField.text);
        parent.debt = ANPDebtField.text;
        parent.phoneNumber = ANPPhoneNumberField.text;
        string jsonData = JsonUtility.ToJson(parent);

        string mail = ANPEmailField.text;
        string mailToAdd = mail.Replace(".", " ");
        tconstants.databaseReference.Child("Parent").Child(ANPFullNameField.text + " -> " + mailToAdd).SetRawJsonValueAsync(jsonData);
        tconstants.isLoaded = false;
        tconstants.load();
        closePopUps = true;
        tabToOpen = 1;
    }

    public void closeANPPopUp() {
        ANPPopUp.SetActive(false);
        ANPEmailField.text = "";
        ANPPasswordField.text = "";
        ANPFullNameField.text = "";
        ANPPhoneNumberField.text = "";
        ANPDebtField.text = "";
        
        ANPEmailField.interactable = true;
        ANPPasswordField.interactable = true;
        ANPFullNameField.interactable = true;
    }

    private void editParent(int parentIndex) {
        showANPPopUp();
        ANPBackButton.SetActive(false);

        ANPEmailField.text = tconstants.parents[parentIndex].email;
        ANPPasswordField.text = "***";
        ANPFullNameField.text = tconstants.parents[parentIndex].fullName;

        ANPEmailField.interactable = false;
        ANPPasswordField.interactable = false;
        ANPFullNameField.interactable = false;

        ANPPhoneNumberField.text = tconstants.parents[parentIndex].phoneNumber;
        ANPDebtField.text = tconstants.parents[parentIndex].debt;
    }

    private void removeParent(int parentIndex) {
        string mail = tconstants.parents[parentIndex].email;
        string mailToAdd = mail.Replace(".", " ");
        tconstants.databaseReference.Child("Parent").Child(tconstants.parents[parentIndex].fullName + " -> " + mailToAdd).SetRawJsonValueAsync(null);
        tconstants.isLoaded = false;
        tconstants.load();

        StartCoroutine(waitAndLoad(1));
    }
    //studentsTab -- end
    
    IEnumerator waitAndLoad(int i) {
        yield return new WaitUntil(() => tconstants.isLoaded == true);
        
        switchTab(i);
    }
}
