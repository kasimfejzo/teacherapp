﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using FlexFramework.Excel;
using UnityEngine;

public class ImportTool : MonoBehaviour {
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private bool importData;
    private Document document;
    // Start is called before the first frame update
    void Start() {
        document = Document.LoadAt("Assets/GameAssets/students.csv");
        if(importData) import();
    }

    private void import() {
        for (int i = 1; i < document.Rows.Count; ++i) {
            string email, password, fullname, phoneNumber, parentEmail;
            email = document[i][1];
            password = "123456";
            fullname = document[i][0];
            phoneNumber = document[i][2];
            parentEmail = document[i][4];
            
            saveStudent(email, password, fullname, phoneNumber, parentEmail);
        }
    }

    private void saveStudent(string email, string password, string fullname, string phoneNumber, string parentEmail) {
        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(email, password)
            .ContinueWith((
                task => {
                    if (task.IsCanceled) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsFaulted) {
                        Firebase.FirebaseException e =
                            task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;
                    
                        LogErrorMessage((AuthError) e.ErrorCode);
                    }

                    if (task.IsCompleted) {
                        print("Registration successful!");
                        saveStudentToDB(email, fullname, phoneNumber, parentEmail);
                    }
                }));
    }
    
    private void LogErrorMessage(AuthError authError) {
        print(authError.ToString());
    }
    
    private void saveStudentToDB(string email, string fullname, string phoneNumber, string parentEmail) {
        Student student = new Student(email, fullname, phoneNumber, parentEmail);
        string jsonData = JsonUtility.ToJson(student);

        string mail = email;
        string mailToAdd = mail.Replace(".", " ");
        tconstants.databaseReference.Child("Student").Child(fullname + " -> " + mailToAdd).SetRawJsonValueAsync(jsonData);
        tconstants.isLoaded = false;
    }
}
