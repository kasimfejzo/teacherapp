﻿using System;
using System.Collections;
using Firebase;
using Firebase.Unity.Editor;
using UnityEngine;
using UnityEngine.UI;

public class ClassesUIController : MonoBehaviour {
    [Header("General")]
    [SerializeField] private TeacherConstants tconstants = null;
    [SerializeField] private GameObject classField = null;
    [SerializeField] private Transform classesListContent = null;
    
    [Header("ANC")]
    [SerializeField] private GameObject ANCPopUp = null;
    [SerializeField] private InputField classNameInput = null;
    [SerializeField] private Transform studentsListContent = null;
    [SerializeField] private GameObject studentField = null;
    [SerializeField] private GameObject ANCBackButton = null;
    
    [Header("ClassUI")]
    [SerializeField] private GameObject classUI = null;
    [SerializeField] private Transform sessionsListContent = null;
    private Class editedClass;
    private Session editedSession;
    
    [Header("ANS")]
    [SerializeField] private GameObject ANSPopUp = null;
    [SerializeField] private InputField sessionNameInputField = null;
    [SerializeField] private InputField sessionDescriptionInputField = null;
    [SerializeField] private GameObject ANSBackButton = null;
    
    
    [Header("SessionUI")]
    [SerializeField] private GameObject sessionUI = null;
    [SerializeField] private Transform sessionTabContent = null;
    [SerializeField] private GameObject editAttachmentURLPopUp = null;
    [SerializeField] private InputField urlInputField = null;
    [SerializeField] private GameObject[] tabButtons = null;

    [Header("SessionTabs")]
    [SerializeField] private GameObject saveAttendanceButton = null;
    [SerializeField] private GameObject sessionTabAddButton = null;
    [SerializeField] private GameObject ANGPopUp = null;
    [SerializeField] private InputField gradeNameInputField = null;
    [SerializeField] private InputField gradeMaxGradeInputField = null;
    [SerializeField] private Transform ANGContent = null;
    [SerializeField] private GameObject studentANGField = null;
    [SerializeField] private GameObject ANAPopUp = null;
    [SerializeField] private InputField assignmentNameInputField = null;
    [SerializeField] private InputField assignmentMaxGradeInputField = null;
    [SerializeField] private Transform ANAContent = null;
    
    [Header("YouTube")]
    [SerializeField] private GameObject youTubeGO = null;
    [SerializeField] private GameObject mainCanvas = null;
    
    
    private int currentClassIndex;
    private int currentSessionIndex;
    
    // Start is called before the first frame update
    void Start() {
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(tconstants.DATABASE_URL);
        StartCoroutine(listAllClasses());
        StartCoroutine(listStudentsToANC(-1));

        currentClassIndex = -1;
        currentSessionIndex = -1;
        editedClass = null;
        editedSession = null;
    }
    
    IEnumerator listAllClasses() {
        yield return new WaitUntil(() => tconstants.isLoaded == true);
        clearContent(classesListContent);

        int index = 0;
        foreach (var c in tconstants.classes) {
            GameObject go = Instantiate(classField, classesListContent);
            go.transform.GetChild(0).GetComponent<Text>().text = c.className;
            int i = index;
            //edit button
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editClass(i); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editClass(i); });
            //edit button -- end
            //remove button
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeClass(i); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { removeClass(i); });
            //remove button -- end
            go.name = index.ToString();
            go.GetComponent<Button>().onClick.AddListener(delegate { openClass(i); });

            ++index;
        }
    }
    private void clearContent(Transform content) {
        try {
            int index = 0;
            Transform child = content.transform.GetChild(index);
            while (child != null) {
                Destroy(child.gameObject);
                ++index;
                child = content.transform.GetChild(index);
            }
        }
        catch (Exception e) {
            Debug.Log(e);
        }
    }

    //ClassesUI
    //ClassField buttons
    private void openClass(int classIndex) { 
        clearContent(sessionsListContent);
        classUI.SetActive(true);
        setupClass(classIndex);
    }

    private void editClass(int classIndex) {
        showANCPopUp();
        ANCBackButton.SetActive(false);
        classNameInput.text = tconstants.classes[classIndex].className;
        if(tconstants.classes[classIndex].sessions.Count > 0) studentsListContent.gameObject.SetActive(false);
        editedClass = tconstants.classes[classIndex];
        StartCoroutine(listStudentsToANC(classIndex));
    }

    private void removeClass(int classIndex) {
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[classIndex].className).SetRawJsonValueAsync(null);
        clearContent(classesListContent);
        tconstants.isLoaded = false;
        tconstants.load();
        StartCoroutine(listAllClasses());
    }
    //ClassField buttons -- end

    //Add new class
    IEnumerator listStudentsToANC(int classIndex) {
        yield return new WaitUntil(() => tconstants.isLoaded == true);
        clearContent(studentsListContent);

        int index = 0;
        foreach (var s in tconstants.students) {
            GameObject go = Instantiate(studentField, studentsListContent);
            go.transform.GetChild(0).GetComponent<Text>().text = s.email;
            go.name = index.ToString();
            ++index;
            
            if (classIndex == -1) continue;
            if (tconstants.classes[classIndex].students.Contains(s.email)) go.transform.GetChild(1).GetComponent<Toggle>().isOn = true;
        }
        
        if(classIndex != -1) removeClass(classIndex);
    }
    public void showANCPopUp() {
        ANCPopUp.SetActive(true);
    }

    public void saveClass() {
        if (classNameInput.text.Equals("")) {
            tconstants.showToast("You need to input a class name in order to continue!", 3);
            return;
        }

        Class c = new Class(classNameInput.text);
        bool studentsSelected = false;
        for (int i = 0; i < studentsListContent.transform.childCount; ++i) {
            if (!studentsListContent.transform.GetChild(i).transform.GetChild(1).GetComponent<Toggle>().isOn) continue;
            
            c.students.Add(studentsListContent.transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text);
            studentsSelected = true;
        }

        if (!studentsSelected) {
            tconstants.showToast("You need to select students for this class!", 3);
            return;
        }
        
        if (editedClass != null) {
            c.sessions = editedClass.sessions;
            editedClass = null;
        }
        
        string jsonData = JsonUtility.ToJson(c);
        tconstants.databaseReference.Child("Class").Child(c.className).SetRawJsonValueAsync(jsonData);
        clearContent(classesListContent);
        clearContent(studentsListContent);
        tconstants.isLoaded = false;
        tconstants.load();
        StartCoroutine(listAllClasses());
        StartCoroutine(listStudentsToANC(-1));
        
        closeANCPopUp();
        ANCBackButton.SetActive(true);
    }

    public void closeANCPopUp() {
        ANCPopUp.SetActive(false);
        classNameInput.text = "";
    }
    //Add new class -- end
    //ClassesUI -- end
    
    //ClassUI
    private void setupClass(int classIndex) {
        classUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = tconstants.classes[classIndex].className;
        int index = 0;
        foreach (var s in tconstants.classes[classIndex].sessions) {
            GameObject go = Instantiate(classField, sessionsListContent); //using classfield cause it's the same shit
            go.transform.GetChild(0).GetComponent<Text>().text = s.sessionName;
            int i = index;
            //edit button
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editSession(i, classIndex); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editSession(i, classIndex); });
            //edit button -- end
            //remove button
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeSession(i, classIndex); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { removeSession(i, classIndex); });
            //remove button -- end
            go.name = index.ToString();
            go.GetComponent<Button>().onClick.AddListener(delegate { openSession(i, classIndex); });
            
            ++index;
        }
    }

    public void closeClass() {
        classUI.SetActive(false);
    }
    
    //SessionField buttons
    private void openSession(int sessionIndex, int classIndex) {
        sessionUI.SetActive(true);
        setupSession(sessionIndex, classIndex);
    }
    
    private void editSession(int sessionIndex, int classIndex) {
        showANSPopUp();
        ANSBackButton.SetActive(false);
        
        sessionNameInputField.text = tconstants.classes[classIndex].sessions[sessionIndex].sessionName;
        sessionDescriptionInputField.text = tconstants.classes[classIndex].sessions[sessionIndex].sessionDescription;

        editedSession = tconstants.classes[classIndex].sessions[sessionIndex];
        removeSession(sessionIndex, classIndex);
    }

    private void removeSession(int sessionIndex, int classIndex) {
        tconstants.classes[classIndex].sessions.RemoveAt(sessionIndex);
        string jsonData = JsonUtility.ToJson(tconstants.classes[classIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[sessionIndex].className).SetRawJsonValueAsync(jsonData);

        clearContent(sessionsListContent);
        setupClass(classIndex);
    }
    
    //SessionField buttons -- end
    
    //Add new session
    public void showANSPopUp() {
        ANSPopUp.SetActive(true);
    }

    public void saveSession() {
        if (sessionNameInputField.text.Equals("") || sessionDescriptionInputField.text.Equals("")) {
            tconstants.showToast("You need to input both name and description in order to continue!", 3);
            return;
        }

        int classIndex = 0;
        foreach (var c in tconstants.classes) {
            if (c.className.Equals(classUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text)) {
                c.sessions.Add(new Session(sessionNameInputField.text, sessionDescriptionInputField.text, tconstants.classes[classIndex].students.Count));
                if (editedSession != null) {
                    c.sessions[c.sessions.Count - 1].documentsUrl = editedSession.documentsUrl;
                    c.sessions[c.sessions.Count - 1].zoomUrl = editedSession.zoomUrl;
                    c.sessions[c.sessions.Count - 1].youtubeUrl = editedSession.youtubeUrl;
                    c.sessions[c.sessions.Count - 1].attendance = editedSession.attendance;
                    c.sessions[c.sessions.Count - 1].tests = editedSession.tests;
                    c.sessions[c.sessions.Count - 1].assignments = editedSession.assignments;

                    editedSession = null;
                }
                string jsonData = JsonUtility.ToJson(c);
                tconstants.databaseReference.Child("Class").Child(c.className).SetRawJsonValueAsync(jsonData);
                break;
            }
            
            ++classIndex;
        }
        
        clearContent(sessionsListContent);
        setupClass(classIndex);
        
        closeANSPopUp();
        ANSBackButton.SetActive(true);
    }

    public void closeANSPopUp() {
        ANSPopUp.SetActive(false);
        sessionNameInputField.text = "";
        sessionDescriptionInputField.text = "";
    }
    //Add new session -- end
    //ClassUI -- end
    
    //SessionUI

    private void setupSession(int sessionIndex, int classIndex) {
        currentClassIndex = classIndex;
        currentSessionIndex = sessionIndex;
        sessionUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = tconstants.classes[classIndex].sessions[sessionIndex].sessionName;
        switchSessionTab(0);
    }

    public void switchSessionTab(int tabIndex) {
        if (currentClassIndex == -1 || currentSessionIndex == -1) return;
        
        clearContent(sessionTabContent);
        saveAttendanceButton.SetActive(false);
        sessionTabAddButton.SetActive(false);
        sessionTabAddButton.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        sessionTabAddButton.transform.GetChild(0).transform.GetComponent<Button>().onClick.RemoveAllListeners();
        switch (tabIndex) {
            case 0:
                setupAttachmentsTab();
                break;
            case 1:
                saveAttendanceButton.SetActive(true);
                setupAttendanceTab();
                break;
            case 2:
                sessionTabAddButton.SetActive(true);
                sessionTabAddButton.transform.GetComponent<Button>().onClick.AddListener(delegate { showANGPopUp(); });
                sessionTabAddButton.transform.GetChild(0).transform.GetComponent<Button>().onClick.AddListener(delegate { showANGPopUp(); });
                setupGradesTab();
                break;
            case 3:
                sessionTabAddButton.SetActive(true);
                sessionTabAddButton.transform.GetComponent<Button>().onClick.AddListener(delegate { showANAPopUp(); });
                sessionTabAddButton.transform.GetChild(0).transform.GetComponent<Button>().onClick.AddListener(delegate { showANAPopUp(); });
                setupAssignmentsTab();
                break;
        }

        foreach (var btn in tabButtons) {
            btn.GetComponent<Image>().color = new Color32(154, 154, 154, 255);
            btn.transform.GetChild(0).GetComponent<Text>().color = new Color32(154, 154, 154, 255);
        }
        tabButtons[tabIndex].GetComponent<Image>().color = new Color32(253, 121, 73, 255);
        tabButtons[tabIndex].transform.GetChild(0).GetComponent<Text>().color = new Color32(253, 121, 73, 255);
    }

    //attachmentTab
    private void setupAttachmentsTab() { //still using classField cause same shit just disable remove option.
        GameObject go1 = Instantiate(classField, sessionTabContent);
        go1.transform.GetChild(0).GetComponent<Text>().text = "Session documents";
        //edit button
        go1.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(0); });
        go1.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(0); });
        //edit button -- end
        //remove button
        go1.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url1 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].documentsUrl;
        go1.GetComponent<Button>().onClick.AddListener(delegate { Application.OpenURL(url1); });
        
        
        GameObject go2 = Instantiate(classField, sessionTabContent);
        go2.transform.GetChild(0).GetComponent<Text>().text = "Zoom meeting";
        //edit button
        go2.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(1); });
        go2.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(1); });
        //edit button -- end
        //remove button
        go2.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url2 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].zoomUrl;
        go2.GetComponent<Button>().onClick.AddListener(delegate { Application.OpenURL(url2); });
        
        
        GameObject go3 = Instantiate(classField, sessionTabContent);
        go3.transform.GetChild(0).GetComponent<Text>().text = "Youtube video";
        //edit button
        go3.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(2); });
        go3.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { showEditAttachmentsUrlPopUp(2); });
        //edit button -- end
        //remove button
        go3.transform.GetChild(2).transform.gameObject.SetActive(false);
        //remove button -- end
        string url3 = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].youtubeUrl;
        go3.GetComponent<Button>().onClick.AddListener(delegate { openVideo(url3); });
        
        
        //TODO Add youtube video display option.
    }
    private void showEditAttachmentsUrlPopUp(int index) {
        editAttachmentURLPopUp.SetActive(true);
        urlInputField.gameObject.name = index.ToString();
        switch (index) {
            case 0:
                urlInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].documentsUrl;
                break;
            case 1:
                urlInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].zoomUrl;
                break;
            case 2:
                urlInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].youtubeUrl;
                break;
        }
    }

    public void saveUrl() {
        switch (int.Parse(urlInputField.gameObject.name)) {
            case 0:
                tconstants.classes[currentClassIndex].sessions[currentSessionIndex].documentsUrl = urlInputField.text;
                break;
            case 1:
                tconstants.classes[currentClassIndex].sessions[currentSessionIndex].zoomUrl = urlInputField.text;
                break;
            case 2:
                tconstants.classes[currentClassIndex].sessions[currentSessionIndex].youtubeUrl = urlInputField.text;
                break;
        }
        
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        closeEditAttachmentsUrlPopUp();
    }

    public void closeEditAttachmentsUrlPopUp() {
        editAttachmentURLPopUp.SetActive(false);
        urlInputField.text = "";
        urlInputField.gameObject.name = "URLInputField";
    }
    //attachmentTab -- end
    
    //attendanceTab
    private void setupAttendanceTab() {
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            GameObject go = Instantiate(studentField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            go.transform.GetChild(1).GetComponent<Toggle>().isOn = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].attendance[i];
            go.name = i.ToString();
        }
    }

    public void saveAttendance() {
        for (int i = 0; i < sessionTabContent.childCount; ++i) {
            tconstants.classes[currentClassIndex].sessions[currentSessionIndex].attendance[i] = sessionTabContent.transform.GetChild(i).transform.GetChild(1).GetComponent<Toggle>().isOn;
        }
        
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        saveAttendanceButton.SetActive(false);
    }
    
    //attendanceTab -- end
    
    //grades
    private void setupGradesTab() {
        int testIndex = 0;
        foreach (var test in tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests) {
            GameObject go = Instantiate(classField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = test.testName;
            int i = testIndex;
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editTest(i); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editTest(i); });
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeTest(i); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editTest(i); });
            ++testIndex;
        }
    }
    
    private void showANGPopUp() {
        ANGPopUp.SetActive(true);
        foreach (var student in tconstants.classes[currentClassIndex].students) {
            GameObject go = Instantiate(studentANGField, ANGContent);
            go.transform.GetChild(0).GetComponent<Text>().text = student;
        }
    }

    public void saveGrade() {
        if (gradeNameInputField.text != "" && gradeMaxGradeInputField.text.Equals("")) {
            tconstants.showToast("You can't save a grade without MAXGRADE", 3);
            return;
        }

        if (gradeNameInputField.text.Equals("") && gradeMaxGradeInputField.text.Equals("")) {
            clearContent(ANGContent);
            gradeNameInputField.text = "";
            gradeMaxGradeInputField.text = "";
            ANGPopUp.SetActive(false);
            return;
        }
        
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            string grade = ANGContent.transform.GetChild(i).transform.GetChild(1).GetComponent<InputField>().text;
            if (grade.Equals("")) grade = "-1";
            if (int.Parse(grade) > int.Parse(gradeMaxGradeInputField.text)) {
                tconstants.showToast("You can't input grades higher than MAXGRADE", 2);
                return;
            }
        }
        
        tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests.Add(new Test(gradeNameInputField.text, int.Parse(gradeMaxGradeInputField.text), tconstants.classes[currentClassIndex].students.Count));
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            string grade = ANGContent.transform.GetChild(i).transform.GetChild(1).GetComponent<InputField>().text;
            if (grade.Equals("")) grade = "-1";
            tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests.Count - 1].studentGrades[i] = int.Parse(grade);
            print(i);
        }
        
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        
        clearContent(ANGContent);
        gradeNameInputField.text = "";
        gradeMaxGradeInputField.text = "";
        ANGPopUp.SetActive(false);
        switchSessionTab(2);
    }

    private void editTest(int testIndex) {
        showANGPopUp();
        clearContent(ANGContent);

        gradeNameInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].testName;
        gradeMaxGradeInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].maxGrade.ToString();

        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            GameObject go = Instantiate(studentANGField, ANGContent);
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            if (tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].studentGrades[i] == -1) continue;
            go.transform.GetChild(1).GetComponent<InputField>().text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests[testIndex].studentGrades[i].ToString();
        }
        
        removeTest(testIndex);
    }

    private void removeTest(int testIndex) {
        tconstants.classes[currentClassIndex].sessions[currentSessionIndex].tests.RemoveAt(testIndex);
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        
        switchSessionTab(2);
    }
    //grades -- end
    
    //assignments
    private void setupAssignmentsTab() {
        int assIndex = 0;
        foreach (var ass in tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments) {
            GameObject go = Instantiate(classField, sessionTabContent);
            go.transform.GetChild(0).GetComponent<Text>().text = ass.testName;
            int i = assIndex;
            go.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { editAssignment(i); });
            go.transform.GetChild(1).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { editAssignment(i); });
            go.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate { removeAssignment(i); });
            go.transform.GetChild(2).transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate { removeAssignment(i); });
            ++assIndex;
        }
    }
    
    private void showANAPopUp() {
        ANAPopUp.SetActive(true);
        foreach (var student in tconstants.classes[currentClassIndex].students) {
            GameObject go = Instantiate(studentANGField, ANAContent);
            go.transform.GetChild(0).GetComponent<Text>().text = student;
        }
    }

    public void saveAssignment() {
        if (assignmentNameInputField.text != "" && assignmentMaxGradeInputField.text.Equals("")) {
            tconstants.showToast("You can't save an assignment without MAXGRADE", 3);
            return;
        }

        if (assignmentNameInputField.text.Equals("") && assignmentMaxGradeInputField.text.Equals("")) {
            clearContent(ANAContent);
            assignmentNameInputField.text = "";
            assignmentMaxGradeInputField.text = "";
            ANAPopUp.SetActive(false);
            return;
        }
        
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            string grade = ANAContent.transform.GetChild(i).transform.GetChild(1).GetComponent<InputField>().text;
            if (grade.Equals("")) grade = "-1";
            if (int.Parse(grade) > int.Parse(assignmentMaxGradeInputField.text)) {
                tconstants.showToast("You can't input grades higher than MAXGRADE", 2);
                return;
            }
        }
        
        tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments.Add(new Test(assignmentNameInputField.text, int.Parse(assignmentMaxGradeInputField.text), tconstants.classes[currentClassIndex].students.Count));
        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            string grade = ANAContent.transform.GetChild(i).transform.GetChild(1).GetComponent<InputField>().text;
            if (grade.Equals("")) continue;
            tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments.Count - 1].studentGrades[i] = int.Parse(grade);
        }
        
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        
        clearContent(ANAContent);
        assignmentNameInputField.text = "";
        assignmentMaxGradeInputField.text = "";
        ANAPopUp.SetActive(false);
        switchSessionTab(3);
    }

    private void editAssignment(int assIndex) {
        showANAPopUp();
        clearContent(ANAContent);

        assignmentNameInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].testName;
        assignmentMaxGradeInputField.text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].maxGrade.ToString();

        for (int i = 0; i < tconstants.classes[currentClassIndex].students.Count; ++i) {
            GameObject go = Instantiate(studentANGField, ANAContent);
            go.transform.GetChild(0).GetComponent<Text>().text = tconstants.classes[currentClassIndex].students[i];
            if (tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].studentGrades[i] == -1) continue;
            go.transform.GetChild(1).GetComponent<InputField>().text = tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments[assIndex].studentGrades[i].ToString();
        }
        
        removeAssignment(assIndex);
    }

    private void removeAssignment(int assIndex) {
        tconstants.classes[currentClassIndex].sessions[currentSessionIndex].assignments.RemoveAt(assIndex);
        string jsonData = JsonUtility.ToJson(tconstants.classes[currentClassIndex]);
        tconstants.databaseReference.Child("Class").Child(tconstants.classes[currentClassIndex].className).SetRawJsonValueAsync(jsonData);
        
        switchSessionTab(3);
    }
    //assignments -- end

    public void closeSession() {
        sessionUI.SetActive(false);
        currentClassIndex = -1;
        currentSessionIndex = -1;
    }
    
    //SessionUI -- end
    
    //yt
    private void openVideo(string url) {
        youTubeGO.SetActive(true);
        mainCanvas.SetActive(false);
        youTubeGO.GetComponent<YoutubePlayer>().youtubeUrl = url;
        youTubeGO.GetComponent<YoutubePlayer>().PlayYoutubeVideo(url);
    }
    public void closeVideoPlayer() {
        youTubeGO.SetActive(false);
        youTubeGO.GetComponent<YoutubePlayer>().Stop();
        mainCanvas.SetActive(true);
    }
    //yt -- end
}